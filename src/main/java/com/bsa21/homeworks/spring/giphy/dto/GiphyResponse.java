package com.bsa21.homeworks.spring.giphy.dto;

import com.fasterxml.jackson.annotation.*;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown=true)
public class GiphyResponse {
    @JsonProperty("data")
    private List<GiphyData> data;

    @Data
    @JsonIgnoreProperties(ignoreUnknown=true)
    public static class GiphyData {
        @JsonProperty("id")
        private String id;
        @JsonProperty("images")
        private GiphyImages images;

        @Data
        @JsonIgnoreProperties(ignoreUnknown=true)
        public static class GiphyImages {
            @JsonProperty("original")
            private GiphyFormat original;

            @Data
            @JsonIgnoreProperties(ignoreUnknown=true)
            public static class GiphyFormat {
                @JsonProperty("url")
                private String url;
            }
        }

    }
}
