package com.bsa21.homeworks.spring.giphy;

import com.bsa21.homeworks.spring.giphy.dto.GiphyResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


@Service
public class GiphyService {

    @Value(value = "${giphy.api_key}")
    private String GIPHY_API_KEY;

    @Value(value = "${giphy.url}")
    private String GIPHY_URL;

    public GiphyResponse searchGif(String query) throws JsonProcessingException {
        var serverUrl = String.format(
                "%s?q=%s&api_key=%s",
                GIPHY_URL,
                query,
                GIPHY_API_KEY);

        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(serverUrl, String.class);
        var json = response.getBody();
        var mapper = new ObjectMapper();
        return mapper.readValue(json, GiphyResponse.class);
    }

    public byte[] downloadGif(GiphyResponse.GiphyData data) {
        var restTemplate = new RestTemplate();
        var response = restTemplate.getForEntity(data.getImages().getOriginal().getUrl(), byte[].class);
        return response.getBody();
    }
}
