package com.bsa21.homeworks.spring.general;

import com.bsa21.homeworks.spring.cache.CacheService;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(headers = {"X-BSA-GIPH"})
public class GeneralController {

    private final CacheService cacheService;

    public GeneralController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping("/gifs")
    public List<String> getAllGifs() throws IOException {
        return cacheService.getAllCache()
                .stream()
                .map(GifsDto::getGifs)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }
}
