package com.bsa21.homeworks.spring.config;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class StartupStorage {

    private final StorageProperties storageProperties;

    public StartupStorage(StorageProperties storageProperties) {
        this.storageProperties = storageProperties;
    }

    @EventListener
    public void create(ContextRefreshedEvent event) throws IOException {
        Files.createDirectories(Paths.get(storageProperties.getPath(), storageProperties.getCache()));
        Files.createDirectories(Paths.get(storageProperties.getPath(), storageProperties.getUsers()));

    }
}
