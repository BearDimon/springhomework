package com.bsa21.homeworks.spring.storage;

import com.bsa21.homeworks.spring.config.StorageProperties;
import com.bsa21.homeworks.spring.user.dto.GifHistoryDto;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class DiskUtil {

    private final StorageProperties storageProperties;

    private final SimpleDateFormat simpleDateFormat;

    public DiskUtil(StorageProperties storageProperties) {
        this.storageProperties = storageProperties;
        this.simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
    }

    public String pathFormat(Path path) {
        return String.format("/%s",
                Paths.get(storageProperties.getPath())
                        .relativize(path)
                        .toString())
                .replace("\\", "/");
    }

    private GifsDto formPathObject(Path path) {
        try {
            return new GifsDto(
                    path.getFileName().toString(),
                    Files.list(path)
                            .map(this::pathFormat)
                            .collect(Collectors.toList()));
        } catch (IOException ignored) {
        }
        return null;
    }

    public void saveGif(byte[] bytes, Path path) throws IOException {
        Files.createDirectories(path.getParent());
        if (!Files.exists(path))
            Files.createFile(path);
        try (FileOutputStream fos = new FileOutputStream(path.toString())) {
            fos.write(bytes);
        }
    }

    public void copyGif(Path from, Path to) throws IOException {
        if (!from.toFile().exists() || to.toFile().exists())
            return;

        if (!to.getParent().toFile().exists())
            Files.createDirectories(to.getParent());

        Files.copy(from, to);
    }

    public List<GifsDto> getDiskStructure(Path path) throws IOException {
        if (!Files.exists(path)) return new ArrayList<>();
        try (var stream = Files.list(path)) {
            return stream
                    .map(this::formPathObject)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    public Optional<GifsDto> getDiskStructureByQuery(Path path, String query) throws IOException {
        if (!Files.exists(path)) return Optional.empty();
        try (var stream = Files.list(path)) {
            return stream
                    .filter(p -> p.getFileName().toString().equals(query))
                    .map(this::formPathObject)
                    .filter(Objects::nonNull)
                    .findAny();
        }
    }

    public void deleteDirs(Path path) throws IOException {
        if (!Files.exists(path)) return;
        try (var stream = Files.list(path)) {
            stream.forEach(p -> {
                try {
                    FileUtils.deleteDirectory(p.toFile());
                } catch (IOException ignored) {
                }
            });
        }
    }

    public void deleteByPath(Path path) throws IOException {
        if (!Files.exists(path)) return;
        if (Files.isDirectory(path))
            FileUtils.deleteDirectory(path.toFile());
        else
            Files.delete(path);
    }

    public List<GifHistoryDto> readUserHistory(Path path) throws IOException {
        if (!Files.exists(path)) return new ArrayList<>();
        try (Stream<String> linesStream = Files.lines(path)) {
            return linesStream
                    .map(p -> p.split(","))
                    .map(p -> {
                        try {
                            return new GifHistoryDto(simpleDateFormat.parse(p[0]), p[1], p[2]);
                        } catch (ParseException ignored) {
                        }
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
    }

    public void writeUserHistory(Path path, GifHistoryDto gifHistoryDto) throws IOException {
        checkHistory(path);
        Files.write(
                path,
                String.format("%s,%s,%s\r\n",
                        simpleDateFormat.format(gifHistoryDto.getDate()),
                        gifHistoryDto.getQuery(),
                        gifHistoryDto.getPath()
                ).getBytes(),
                StandardOpenOption.APPEND);
    }

    public void checkHistory(Path path) throws IOException {
        if (!Files.exists(path)) {
            Files.createDirectories(path.getParent());
            Files.createFile(path);
        }
    }
}
