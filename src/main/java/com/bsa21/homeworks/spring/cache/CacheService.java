package com.bsa21.homeworks.spring.cache;

import com.bsa21.homeworks.spring.config.StorageProperties;
import com.bsa21.homeworks.spring.storage.DiskUtil;
import com.bsa21.homeworks.spring.giphy.GiphyService;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class CacheService {

    private final DiskUtil diskUtil;
    private final GiphyService giphyService;
    private final Path CACHE_PATH;
    private final Random random;

    public CacheService(DiskUtil diskUtil, GiphyService giphyService, StorageProperties storageProperties) {
        this.diskUtil = diskUtil;
        this.giphyService = giphyService;
        CACHE_PATH = Paths.get(storageProperties.getPath(), storageProperties.getCache());
        this.random = new Random();
    }

    public List<GifsDto> getAllCache() throws IOException {
        return diskUtil.getDiskStructure(CACHE_PATH);
    }

    public Optional<GifsDto> getCache(String query) throws IOException {
        return diskUtil.getDiskStructureByQuery(CACHE_PATH, query);
    }

    public GifsDto generateCache(String query) throws IOException {
        var giphy = giphyService.searchGif(query).getData();
        var response = giphy.get(random.nextInt(giphy.size()));
        var bytes = giphyService.downloadGif(response);
        diskUtil.saveGif(bytes,
                Paths.get(CACHE_PATH.toString(), query, response.getId() + ".gif"));
        return diskUtil.getDiskStructureByQuery(CACHE_PATH, query).orElseThrow();
    }

    public void cleanCache() throws IOException {
        diskUtil.deleteDirs(CACHE_PATH);
    }
}
