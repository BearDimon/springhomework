package com.bsa21.homeworks.spring.cache.dto;

import lombok.Data;

@Data
public class CacheGenerateDto {
    private String query;
}
