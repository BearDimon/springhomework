package com.bsa21.homeworks.spring.cache;

import com.bsa21.homeworks.spring.cache.dto.CacheGenerateDto;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping(value = "/cache", headers = {"X-BSA-GIPH"})
public class CacheController {

    private final CacheService cacheService;

    public CacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping()
    public List<GifsDto> getCache(@PathParam("query") String query) throws IOException {
        return query == null ?
                cacheService.getAllCache() :
                cacheService.getCache(query).stream().collect(Collectors.toList());
    }

    @PostMapping("/generate")
    public GifsDto generateGif(@RequestBody CacheGenerateDto cacheGenerateDto) throws IOException {
        return cacheService.generateCache(cacheGenerateDto.getQuery());
    }

    @DeleteMapping()
    public void deleteCache() throws IOException {
        cacheService.cleanCache();
    }
}
