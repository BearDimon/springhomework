package com.bsa21.homeworks.spring.user;

import com.bsa21.homeworks.spring.user.dto.GifHistoryDto;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import com.bsa21.homeworks.spring.user.dto.UserGifInfoDto;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.constraints.Pattern;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/user/{id}", headers = {"X-BSA-GIPH"})
@Validated
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/all")
    public List<GifsDto> getUserGifs(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id) throws IOException {
        return userService.getUserGifs(id);
    }

    @GetMapping("/history")
    public List<GifHistoryDto> getUserHistory(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id) throws IOException {
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/history/clean")
    public void cleanUserHistory(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id) throws IOException {
        userService.deleteUserHistory(id);
    }

    @GetMapping("/search")
    public String getUserGif(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id ,
                             @PathParam("query") String query,
                             @PathParam("force") boolean force) throws IOException {
        var res = userService.searchUserGif(id, new UserGifInfoDto(query, force));
        if (res.isPresent())
            return res.get();
        else
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);

    }

    @PostMapping("/generate")
    public String generateUserGif(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id, @RequestBody UserGifInfoDto gifInfoDto) throws IOException {
        return userService.generateUserGif(id, gifInfoDto);
    }

    @DeleteMapping("/reset")
    public void cleanUserCache(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id, @PathParam("query") String query) {
        userService.deleteUserMemory(id, query);
    }

    @DeleteMapping("/clean")
    public void cleanUserData(@PathVariable("id") @Pattern(regexp = "^[^\\\\/?%*:|\"<>.]+$") String id) throws IOException {
        userService.deleteUser(id);
    }
}
