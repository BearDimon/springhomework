package com.bsa21.homeworks.spring.user;

import com.bsa21.homeworks.spring.cache.CacheService;
import com.bsa21.homeworks.spring.config.StorageProperties;
import com.bsa21.homeworks.spring.storage.DiskUtil;
import com.bsa21.homeworks.spring.user.dto.GifHistoryDto;
import com.bsa21.homeworks.spring.user.dto.GifsDto;
import com.bsa21.homeworks.spring.user.dto.UserGifInfoDto;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

@Service
public class UserService {

    private final DiskUtil diskUtil;
    private final CacheService cacheService;
    private final StorageProperties storageProperties;
    private final String USERS_PATH;
    private final Random random;

    private final Map<String, Map<String, List<String>>> memoryCache;

    public UserService(DiskUtil diskUtil, CacheService cacheService, StorageProperties storageProperties) {
        this.diskUtil = diskUtil;
        this.cacheService = cacheService;
        this.storageProperties = storageProperties;
        this.USERS_PATH = Paths.get(storageProperties.getPath(), storageProperties.getUsers()).toString();
        this.memoryCache = new HashMap<>();
        this.random = new Random();
    }

    public List<GifsDto> getUserGifs(String id) throws IOException {
        return diskUtil.getDiskStructure(Paths.get(USERS_PATH, id));
    }

    public List<GifHistoryDto> getUserHistory(String id) throws IOException {
        return diskUtil.readUserHistory(
                Paths.get(USERS_PATH, id, storageProperties.getHistory()));
    }

    public void deleteUserHistory(String id) throws IOException {
        var path = Paths.get(USERS_PATH, id, storageProperties.getHistory());
        diskUtil.deleteByPath(path);
        diskUtil.checkHistory(path);
    }

    public Optional<String> searchUserGif(String id, UserGifInfoDto gifInfoDto) throws IOException {
        var userCache = memoryCache.get(id);
        if (!gifInfoDto.isForce()) {
            if (userCache != null && userCache.containsKey(gifInfoDto.getQuery())) {
                var cache = userCache.get(gifInfoDto.getQuery());
                return Optional.of(cache.get(random.nextInt(cache.size())));
            }
        }

        var userPath = Paths.get(USERS_PATH, id);

        var result = diskUtil.getDiskStructureByQuery(userPath, gifInfoDto.getQuery());

        if (result.isPresent()) {
            if (userCache == null) {
                memoryCache.put(id, new HashMap<>());
                userCache = memoryCache.get(id);
            }
            userCache.put(gifInfoDto.getQuery(), result.get().getGifs());
            var gifs = result.get().getGifs();
            return Optional.of(gifs.get(random.nextInt(gifs.size())));
        }

        return Optional.empty();
    }

    public String generateUserGif(String id, UserGifInfoDto gifInfoDto) throws IOException {
        Optional<GifsDto> cacheRes = Optional.empty();
        if (!gifInfoDto.isForce()) {
            cacheRes = cacheService.getCache(gifInfoDto.getQuery());
        }
        if (cacheRes.isPresent()) {
            var gifs = cacheRes.get().getGifs();
            var path = Paths.get(storageProperties.getPath(), gifs.get(random.nextInt(gifs.size())));
            var newUserGifPath = Paths.get(USERS_PATH, id,
                    gifInfoDto.getQuery(),
                    path.getFileName().toString());
            var newUserGifPathStr = diskUtil.pathFormat(newUserGifPath);

            diskUtil.copyGif(path,
                    newUserGifPath);

            if (memoryCache.containsKey(id)) {
                if (memoryCache.get(id).containsKey(gifInfoDto.getQuery()))
                    memoryCache.get(id).get(gifInfoDto.getQuery()).add(newUserGifPathStr);
                else {
                    var list = new ArrayList<String>();
                    list.add(newUserGifPathStr);
                    memoryCache.get(id).put(gifInfoDto.getQuery(), list);
                }
            } else {
                var list = new ArrayList<String>();
                list.add(newUserGifPathStr);
                var map = new HashMap<String, List<String>>();
                map.put(gifInfoDto.getQuery(), list);
                memoryCache.put(id, map);
            }

            diskUtil.writeUserHistory(
                    Paths.get(storageProperties.getPath(), storageProperties.getUsers(), id, storageProperties.getHistory()),
                    new GifHistoryDto(Calendar.getInstance().getTime(), gifInfoDto.getQuery(), newUserGifPathStr));

            return newUserGifPathStr;
        } else {
            cacheService.generateCache(gifInfoDto.getQuery());
            gifInfoDto.setForce(false);

            return generateUserGif(id, gifInfoDto);
        }
    }

    public void deleteUserMemory(String id, String query) {
        var userCache = memoryCache.get(id);
        if (userCache == null) return;
        if (query == null)
            memoryCache.remove(id);
        else
            userCache.remove(query);
    }

    public void deleteUser(String id) throws IOException {
        memoryCache.remove(id);
        diskUtil.deleteByPath(Paths.get(USERS_PATH, id));
    }
}
