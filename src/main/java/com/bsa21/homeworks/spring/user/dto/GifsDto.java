package com.bsa21.homeworks.spring.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GifsDto {
    private String query;
    private List<String> gifs;
}
