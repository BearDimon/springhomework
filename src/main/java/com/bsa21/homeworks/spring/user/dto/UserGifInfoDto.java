package com.bsa21.homeworks.spring.user.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserGifInfoDto {
    private String query;
    private boolean force;
}
